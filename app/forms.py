from flask_wtf import FlaskForm
from wtforms import validators, IntegerField, SubmitField, TextField, SelectField
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms.validators import DataRequired, ValidationError
from .models import Student, Staff


class DefaultForm(FlaskForm):
    load = IntegerField('Default load', validators=[DataRequired()])

class EditStaffForm(FlaskForm):
    name = TextField('Name', validators=[DataRequired()])
    load = IntegerField('Default load', validators=[DataRequired()])

class EditStudentForm(FlaskForm):
    pref1 = TextField('Preference 1')
    pref2 = TextField('Preference 2')
    pref3 = TextField('Preference 3')
    pref4 = TextField('Preference 4')

class EditProjectForm(FlaskForm):
    title = TextField('Title')
    load = IntegerField('Default load', validators=[DataRequired()])
    staff_proposed = TextField('Staff proposed', id='staff_autocomplete')


class AllocationForm(FlaskForm):
    project = TextField('Project name', id='project_autocomplete')
    supervisor = TextField('Supervisor name', id='staff_autocomplete')

class StatusForm(FlaskForm):
    status = SelectField('status', choices=[(0, ''),(1,'suitable'),(2,'unsuitable'), (3,'unchecked')], validators=[DataRequired()])

class AddStudentForm(FlaskForm):
    name = TextField('Name', validators=[DataRequired()])
    username = TextField('Username', validators=[DataRequired()])
    pref1 = TextField('Preference 1', validators=[DataRequired()])
    pref2 = TextField('Preference 2', validators=[DataRequired()])
    pref3 = TextField('Preference 3', validators=[DataRequired()])
    pref4 = TextField('Preference 4', validators=[DataRequired()])
