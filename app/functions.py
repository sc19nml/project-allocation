# define a function to used later
from .models import Student, Staff, Project

"""
These funtions are implemented in the current project (developed by Project Coordinator - David Head)
They deal with project codes & staff codes

"""

def staffForCode( code ):
    """
    Tries to guess the staff member for the given short project code;
    usually the first new letters.
    """

    # 'OWN' projects.
    if code.lower().startswith("own")	: return "<unknown>"

    # Companies.
    if code.lower().startswith("elder")	: return "<unknown>"
    if code.lower().startswith("herd")	: return "<unknown>"

    # Staff initials. Check Marc de Kamps before Mehmet.
    if code.lower().startswith("mdk")	: return "Marc de Kamps"

    if code.lower().startswith("bb")	: return "Brandon Bennett"
    if code.lower().startswith("twak")	: return "Tom Kelly"
    if code.lower().startswith("nde")	: return "Nick Efford"
    if code.lower().startswith("dah")	: return "David Head"
    if code.lower().startswith("mas")	: return "Ammar Alsalka"
    if code.lower().startswith("hac")	: return "Hamish Carr"
    if code.lower().startswith("oaj")	: return "Owen Johnson"
    if code.lower().startswith("kv")	: return "Kristina Vuskovic"
    if code.lower().startswith("kd")	: return "Karim Djemame"
    if code.lower().startswith("rr")	: return "Roy Ruddle"
    if code.lower().startswith("dch")	: return "David Hogg"
    if code.lower().startswith("ns")	: return "Natasha Shakhlevich"
    if code.lower().startswith("jx")	: return "Jie Xu"
    if code.lower().startswith("ajb")	: return "Andy Bulpitt"
    if code.lower().startswith("sw")	: return "Sam Wilson"
    if code.lower().startswith("kl")	: return "Kelvin Lau"
    if code.lower().startswith("fl")	: return "Farha Lakhani"
    if code.lower().startswith("zw")	: return "Zheng Wang"
    if code.lower().startswith("md")	: return "Mehmet Dogar"
    if code.lower().startswith("tl")	: return "Toni Lassila"
    if code.lower().startswith("hw")	: return "He Wang"
    if code.lower().startswith("mw")	: return "Mark Walkley"
    if code.lower().startswith("ea")	: return "Eric Atwell"
    if code.lower().startswith("aa")	: return "Abdulrahman Altahhan"
    if code.lower().startswith("tr")	: return "Thomas Ranner"
    if code.lower().startswith("al")	: return "Amy Lowe"
    if code.lower().startswith("so")	: return "Sebastian Ordyniak"
    if code.lower().startswith("jl")	: return "Joanna Leng"

    # Staff surnames.
    if code.lower().startswith("cohn")	: return "Tony Cohn"
    if code.lower().startswith("kwan")	: return "Raymond Kwan"
    if code.lower().startswith("dimit")	: return "Vania Dimitrova"
    if code.lower().startswith("adler")	: return "Isolde Adler"
    if code.lower().startswith("cohen")	: return "Netta Cohen"
    if code.lower().startswith("stell")	: return "John Stell"

    # Common mistakes.
    if code.lower().startswith("dha")	: return "David Head"
    if code.lower().startswith("0aj")	: return "Owen Johnson"
    if code.lower().startswith("w01")	: return "Sam Wilson"
    if code.lower().startswith("he07")	: return "He Wang"

    return ("<none>")


def normaliseCode( code ):
    """Normalises the code to something short that students will not change.
    Will add additional checks year-by-year as students find new and
    imaginative ways to confound my expectations ..."""

    # First, remove any numbers near the start (which some students manage to do).
    if code[0].isdigit():

        # Could be: 1. , 2., .... and so on. Find the first space or other delimiter.
        if "." in code:
            code = code[code.find(".")+1:]

    # Remove any non-alphanumeric characters.
    normed = ""
    for char in code:
        if char.isalnum():
            normed += char

    return normed.strip().upper()


def swap( pcode ):
    maxLen = 8
    # Returns the current set of long project codes.
    if (len(pcode) > maxLen):
        # Find a short code that does not exist in the set of codes.
        suffix = 0
        while Project.query.filter(Project.code == "{0}{1}".format(pcode[:maxLen-1],suffix)).first() != None:
            suffix += 1
        # Swap code and save to database
        pcode = "{0}{1}".format(pcode[:maxLen-1],suffix)
    return pcode
