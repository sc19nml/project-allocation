from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from flask_admin import Admin
# from werkzeug.middleware.profiler import ProfilerMiddleware

#create an application object (of type Flask)
app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

migrate = Migrate(app, db, render_as_batch=True)

admin = Admin(app,template_mode='bootstrap3')

# app.wsgi_app = ProfilerMiddleware(
#     app.wsgi_app, restrictions=[30], profile_dir=".", filename_format="{method}.{path}.{elapsed:06f}ms.{time:f}.prof"
# )

from app import views, models
