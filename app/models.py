from app import db

class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    stu_name = db.Column(db.String(50), index=True)
    username = db.Column(db.String(50), index=True, unique=True)
    pref1 = db.Column(db.String(50), index=True)
    pref2 = db.Column(db.String(50), index=True)
    pref3 = db.Column(db.String(50), index=True)
    pref4 = db.Column(db.String(50), index=True)
    allocate = db.Column(db.Integer, default=0)
    projects = db.Column(db.String(50), db.ForeignKey('project.code'))
    staff_name = db.Column(db.String(50), db.ForeignKey('staff.name'))

    degree = db.Column(db.String(50))
    time = db.Column(db.DateTime)
    title1 = db.Column(db.String(50), index=True)
    title2 = db.Column(db.String(50), index=True)
    title3 = db.Column(db.String(50), index=True)
    title4 = db.Column(db.String(50), index=True)
    reason1 = db.Column(db.String(500))
    reason2 = db.Column(db.String(500))
    reason3 = db.Column(db.String(500))
    reason4 = db.Column(db.String(500))
    own_title = db.Column(db.String(50), default='None')
    own_area = db.Column(db.String(100), default='None')
    own_staff = db.Column(db.String(100), default='None')
    own_resource = db.Column(db.String(200), default='None')
    own_description = db.Column(db.Text, default='None')
    own_status = db.Column(db.Integer, default=0)

    def __repr__(self):
        return '{}{}{}{}'.format(self.id, self.stu_name, self.username, self.allocate)

    # status of OWN project
    def __status__(self):
        if (self.own_status == 1):
            return 'suitable'
        if (self.own_status == 2):
            return 'unsuitable'
        else:
            return 'unchecked'

    # to indicate the project assigned is of student 1st choice or 2nd or 3rd or 4th
    def __pref__(self):
        if (self.projects != None):
            if (self.projects == self.pref1):
                self.allocate = 1
            elif (self.projects == self.pref2):
                self.allocate = 2
            elif (self.projects == self.pref3):
                self.allocate = 3
            elif (self.projects == self.pref4):
                self.allocate = 4
            else:
                self.allocate = 5
        else:
            self.allocate = 0
        db.session.commit()
        return self.allocate

    # to obtain the title of a project from its code
    def __codeToTitle__(self, code):
        project = Project.query.filter_by(code=code).first()
        return project.title


class Staff(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500), index=True, unique=True)
    load = db.Column(db.Integer, default=1)
    current = db.Column(db.Integer, default=0)
    projects = db.relationship('Project', backref='project', lazy='dynamic')
    students = db.relationship('Student', backref='staff', lazy='dynamic')

    def __repr__(self):
        return '{}{}{}'.format(self.id, self.name, self.load, self.current)

    # to update current number of students assigned without multiple additions/subtraction
    def current_update(self, count):
        self.current = count
        db.session.commit()
        return self.current


class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(50), index=True, unique=True)
    title = db.Column(db.String(100))
    popularity = db.Column(db.Integer, default=0)
    load = db.Column(db.Integer, default=1)
    current = db.Column(db.Integer, default=0)
    students = db.relationship('Student', backref='project', lazy='dynamic')
    staff_proposed = db.Column(db.String(50))
    staff_assigned = db.Column(db.String(50), db.ForeignKey('staff.name'))
    anystaff = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '{}{}{}{}'.format(self.id, self.code, self.popularity, self.proposal)

    def to_dict(self):
        return {
            'code': self.code,
            'title': self.title,
            'load': self.load,
            'current': self.students.count(),
            'available': self.load - self.students.count()
        }

    # to update current number of students assigned without multiple additions/subtraction
    def current_update(self, count):
        self.current = count
        db.session.commit()
        return self.current
