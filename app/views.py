"""
The views module will contain code that produces dynamic content
that will be presented to the user when they request it
"""

from flask import render_template, flash, redirect, url_for, request
from flask import Flask, Response
from flask_admin.contrib.sqla import ModelView
from app import app, db, admin
from sqlalchemy import func
from .forms import DefaultForm, EditStaffForm, EditStudentForm, StatusForm, AllocationForm, EditProjectForm, AddStudentForm
from .models import Student, Staff, Project
from .functions import staffForCode, normaliseCode, swap
from config import basedir

import os
from os.path import join, dirname, realpath
import pandas as pd

# We need to import JSON in order to parse the data sent by the JS
import json


@app.route('/')
def home():
    return render_template('home.html')


# Get the uploaded files
@app.route("/", methods=['POST'])
def uploadFiles():
      # get the uploaded file
      uploaded_file = request.files['file']
      if '.csv' not in uploaded_file.filename:
          flash('File submitted is not of type .csv')  # check if file is of type csv
      elif uploaded_file.filename != '':
          file_path = os.path.join(basedir,'app/static/files', uploaded_file.filename)
          uploaded_file.save(file_path) # save the file
          return redirect(url_for('parseFiles',  file_name = uploaded_file.filename))
      return redirect(url_for('home'))

# process the data in the file
@app.route("/parse/<file_name>", methods=('GET', 'POST'))
def parseFiles(file_name):
    file_path = os.path.join(basedir,'app/static/files', file_name)
    # Use Pandas to parse the CSV file
    csvData = pd.read_csv(file_path)

    student = Student.query.order_by(Student.id).first()

    # for 1st round of allocation
    if student is None:
        # Loop through the rows
        for i, row in csvData.iterrows():
            # for student
            username = row[3][0:row[3].find('@')] # strip username from email address

            # check for mistakes in preference code submitted by students and return the correct version
            file_pref1 = normaliseCode(row[9])
            file_pref2 = normaliseCode(row[12])
            file_pref3 = normaliseCode(row[15])
            file_pref4 = normaliseCode(row[18])

            # shorten long project codes and keep them unique
            if (file_pref1 != file_pref2 != file_pref3 != file_pref4):
                file_pref1 = swap(file_pref1)
                file_pref2 = swap(file_pref2)
                file_pref3 = swap(file_pref3)
                file_pref4 = swap(file_pref4)

            # add student record to the database
            stu = Student(stu_name=row[4], username=username, degree = row[7],
                pref1=file_pref1, title1 = row[10], reason1 = row[11],
                pref2=file_pref2, title2 = row[13], reason2 = row[14],
                pref3=file_pref3, title3 = row[16], reason3 = row[17],
                pref4=file_pref4, title4 = row[19], reason4 = row[20],
                own_title = row[21], own_area = row[22],
                own_staff = row[23], own_resource = row[24],
                own_description = row[25])
            db.session.add(stu)
            db.session.commit()
        flash('File uploaded successfully')
        return redirect(url_for('home'))

    # for 2nd round (if some student records already exists)
    else:
        # Loop through the rows
        for i, row in csvData.iterrows():
            # strip username from email address
            username = row[3][0:row[3].find('@')]
            s = Student.query.filter(Student.username==username).first()

            # check for mistakes in preference code submitted by students and return the correct version
            file_pref1 = normaliseCode(row[9])
            file_pref2 = normaliseCode(row[12])
            file_pref3 = normaliseCode(row[15])
            file_pref4 = normaliseCode(row[18])

            # shorten long project codes and keep them unique
            if (file_pref1 != file_pref2 != file_pref3 != file_pref4):
                file_pref1 = swap(file_pref1)
                file_pref2 = swap(file_pref2)
                file_pref3 = swap(file_pref3)
                file_pref4 = swap(file_pref4)

            """
            Checking each student:
            if student have already existed in database, update preferences
            else, add a new student's record to the database
            """

            if (s is not None):
                #username already existed in the table, update student's record and unallocate student
                s.pref1 = file_pref1
                s.pref2 = file_pref2
                s.pref3 = file_pref3
                s.pref4 = file_pref4

            else:
                stu = Student(stu_name=row[4], username=username, degree = row[7],
                    pref1=file_pref1, title1 = row[10], reason1 = row[11],
                    pref2=file_pref2, title2 = row[13], reason2 = row[14],
                    pref3=file_pref3, title3 = row[16], reason3 = row[17],
                    pref4=file_pref4, title4 = row[19], reason4 = row[20],
                    own_title = row[21], own_area = row[22],
                    own_staff = row[23], own_resource = row[24],
                    own_description = row[25])
                db.session.add(stu)
            db.session.commit()
        flash('File uploaded successfully')
        return redirect(url_for('home'))

    return redirect(url_for('home'))

@app.route('/addProject')
def pop():
    """
    Check every project listed in Student's table.
    If project exists in Project's table, popularity increases by 1
    If not, add a new project record to Project's table
    """
    student =  Student.query.all()

    for s in student:
        # try to query project code in the database
        try:
            p1 = Project.query.filter(Project.code==s.pref1).first()
            p1.popularity = p1.popularity+1
        # if error, project code does not exist, add one
        except:
            a1 = Project(code=s.pref1, title=s.title1, staff_proposed=staffForCode(s.pref1))
            db.session.add(a1)
        db.session.commit()

        try:
            p2 = Project.query.filter(Project.code==s.pref2).first()
            p2.popularity = p2.popularity+1
        except:
            a2 = Project(code=s.pref2, title=s.title2, staff_proposed=staffForCode(s.pref2))
            db.session.add(a2)
        db.session.commit()

        try:
            p3 = Project.query.filter(Project.code==s.pref3).first()
            p3.popularity = p3.popularity+1
        except:
            a3 = Project(code=s.pref3, title=s.title3, staff_proposed=staffForCode(s.pref3))
            db.session.add(a3)
        db.session.commit()

        try:
            p4 = Project.query.filter(Project.code==s.pref4).first()
            p4.popularity = p4.popularity+1
        except:
            a4 = Project(code=s.pref4, title=s.title4, staff_proposed=staffForCode(s.pref4))
            db.session.add(a4)
        db.session.commit()
    return redirect(url_for('displayProject'))


@app.route('/project')
def displayProject():
    # query all projects in database to display in template
    project =  Project.query.all()
    return render_template("allProject.html",
                            title='All Projects',
                            project=project)


@app.route('/editProject/<p_id>', methods=('GET', 'POST'))
def editProject(p_id):
    p =  Project.query.filter_by(id=p_id).first()
    # form has 2 parameters, obj is a project object with p_id
    # so that the current information of the project can be shown in the form for edition
    form = EditProjectForm(formdata=request.form, obj=p)
    if form.validate_on_submit():
        p.title = form.title.data
        p.load = form.load.data
        p.staff_proposed = form.staff_proposed.data
        db.session.commit()
        flash('Changes saved')
        return redirect(url_for('displayProject'))
    return render_template('editProject.html',title='Edit project',form=form, p=p)

@app.route('/deleteProject/<p_id>')
def deleteProject(p_id):
    # delete a single project, identified by a uniqe id
    p =  Project.query.get(p_id)
    db.session.delete(p)
    db.session.commit()
    return redirect(url_for('displayProject'))

@app.route('/resetProject')
def resetProject():
    # query to delete all records in Project table
    Project.query.delete()
    db.session.commit()
    flash('All project records deleted')
    return redirect(url_for('displayProject'))


@app.route('/student')
def displayStudent():
    student =  Student.query.order_by(Student.id).all()
    return render_template("allStudent.html",
                            title='All Students',
                            student=student)

@app.route('/deleteStudent/<stu_id>')
def deleteStudent(stu_id):
    student =  Student.query.filter_by(id=stu_id).first()
    db.session.delete(student)
    db.session.commit()
    return redirect(url_for('displayStudent'))

@app.route('/editStudent/<stu_id>', methods=('GET', 'POST'))
def editStudent(stu_id):
    student =  Student.query.filter_by(id=stu_id).first()
    form = EditStudentForm(formdata=request.form, obj=student)
    if form.validate_on_submit():
        student.pref1 = form.pref1.data
        student.pref2 = form.pref2.data
        student.pref3 = form.pref3.data
        student.pref4 = form.pref4.data
        student.allocate = form.allocate.data
        student.staff_name = form.supervisor.data
        db.session.commit()
        staff =  Staff.query.filter_by(id=int(student.staff.id)).first()
        staff.current = staff.current + 1
        db.session.commit()
        return redirect(url_for('displayStudent'))
    return render_template('editStudent.html',title='Edit student',form=form, student=student)

@app.route('/addOneStudent', methods=('GET', 'POST'))
def addOneStudent():
    form = AddStudentForm(formdata=request.form)
    if form.validate_on_submit():
        try:
            s = Student(stu_name = form.name.data, username = form.username.data,
                        pref1 = form.pref1.data, pref2 = form.pref2.data,
                        pref3 = form.pref3.data, pref4 = form.pref4.data)
            db.session.add(s)
            db.session.commit()
            flash(form.name.data + ' added successfully')
            return redirect(url_for('displayStudent'))
        except:
            flash('Username already exists, try again')
    return render_template('addStudent.html',title='Add a student',form=form)


@app.route('/resetStudent')
def resetStudent():
    Student.query.delete()
    db.session.commit()
    flash('All student records deleted')
    return redirect(url_for('displayStudent'))

"""
 Show each student's profile, and allow project-coordinator to:
 Allocate a project and a supervisor to this student
 If a student proposed an OWN project, set OWN project status to uncheck/suitable/unsuitable
"""
@app.route('/student/<username>', methods=('GET', 'POST'))
def eachStudent(username):
    student =  Student.query.filter_by(username=username).first()
    form1 = StatusForm(formdata=request.form)
    form2 = AllocationForm(formdata=request.form)

    if (form1.validate_on_submit() and form1.status.data != '0'):
        # update own project status: unchecked (by default), suitable, unsuitable
        student.own_status = form1.status.data
        db.session.commit()
        flash('OWN project status changed')
        return redirect(request.url)

    if form2.validate_on_submit():
        # check user's input
        if (form2.project.data == '' or form2.supervisor.data == ''
        or form2.project.data == 'None' and form2.supervisor.data != 'None'
        or form2.project.data != 'None' and form2.supervisor.data == 'None' ):
            flash('Project name and supervisor name required')
            return redirect(request.url)

        # deallocate a student
        elif (form2.project.data == 'None' and form2.supervisor.data == 'None'):
            try:
                project = Project.query.filter_by(code = student.projects).first()
                project.staff_assigned = None
                student.projects = None
                student.staff_name = None
                db.session.commit()
                flash('Deallocate for ' + student.stu_name)
                return redirect(request.url)
            except:
                flash('Deallocate does not work: this student have not yet assigned to any project')
                return redirect(request.url)

        # allocation process
        else:
            # get input from user and check if staff name/ project name exists in database
            staff =  Staff.query.filter(Staff.name==form2.supervisor.data).first()
            project = Project.query.filter(Project.code==form2.project.data).first()

            # check if staff name exists (to eliminate spelling error cases)
            if (staff is not None):
                # if project doesn't exist (i.e. project assigned is not 1 of student's preference), add new project to project list
                if project is None:
                    a = Project(code=form2.project.data, staff_proposed=staffForCode(form2.project.data))
                    db.session.add(a)
                    db.session.commit()
                    flash('New project has been added to database')

                # if both project and staff exists in database, update the following
                # allocate a project to a student (save project name in student's object)
                student.projects = form2.project.data
                # allocate a staff to a student (save staff name in student's object)
                student.staff_name = form2.supervisor.data

                # update current number of students assigned to the staff
                staff.current = staff.current + 1

                # update the link between project and staff: a staff has been assigned to supervise this project
                p = Project.query.filter(Project.code==form2.project.data).first()
                p.staff_assigned = form2.supervisor.data
                p.current = p.current + 1

                # save all changes to the database
                db.session.commit()
                flash('Allocate successfully')
            else:
                flash('Warning: Staff name does not exist!')
            return redirect(request.url)

    return render_template("studentProfile.html",
                            title='Each student profile',
                            student=student, form1=form1, form2=form2)

@app.route('/staff', methods=('GET', 'POST'))
def displayStaff():
    form = DefaultForm()
    staff =  Staff.query.order_by(Staff.id).all()
    sum_load = Staff.query.with_entities(func.sum(Staff.load).label('maxload')).first().maxload
    sum_students = Staff.query.with_entities(func.sum(Staff.current).label('students')).first().students
    return render_template("allStaff.html",
                            title='All Staffs', form = form,
                            staff=staff, sum_load=sum_load, sum_students=sum_students)

@app.route('/deleteStaff/<staff_id>')
def deleteStaff(staff_id):
    staff =  Staff.query.filter_by(id=staff_id).first()
    db.session.delete(staff)
    db.session.commit()
    return redirect(url_for('displayStaff'))

@app.route('/resetStaff')
def resetStaff():
    Staff.query.delete()
    db.session.commit()
    flash('All staff member records deleted')
    return redirect(url_for('displayStaff'))


@app.route('/editStaff/<staff_id>', methods=('GET', 'POST'))
def editStaff(staff_id):
    staff =  Staff.query.filter_by(id=staff_id).first()
    form = EditStaffForm(formdata=request.form, obj=staff)
    if form.validate_on_submit():
        staff.name = form.name.data
        staff.load = form.load.data
        db.session.commit()
        flash('Changes saved')
        return redirect(url_for('displayStaff'))
    return render_template('editStaff.html',title='Edit staff',form=form, staff=staff)

@app.route('/addOneStaff', methods=('GET', 'POST'))
def addOneStaff():
    form = EditStaffForm(formdata=request.form)
    if form.validate_on_submit():
        s_check = Staff.query.filter(Staff.name==form.name.data).first()
        if (s_check != None):
            flash('Staff name already exist, try again')
        else:
            s = Staff(name = form.name.data, load = form.load.data)
            db.session.add(s)
            db.session.commit()
            flash(form.name.data + ' added successfully')
            return redirect(url_for('displayStaff'))
    return render_template('addStaff.html',title='Add a staff member',form=form)


@app.route('/addStaff')
def addStaff():
    # save staff name
    try:
        db.session.add(Staff(name = "Marc de Kamps"))
        db.session.add(Staff(name = "Brandon Bennett"))
        db.session.add(Staff(name = "Tom Kelly"))
        db.session.add(Staff(name = "Nick Efford"))
        db.session.add(Staff(name = "David Head"))
        db.session.add(Staff(name = "Ammar Alsalka"))
        db.session.add(Staff(name = "Hamish Carr"))
        db.session.add(Staff(name = "Owen Johnson"))
        db.session.add(Staff(name = "Kristina Vuskovic"))
        db.session.add(Staff(name = "Karim Djemame"))
        db.session.add(Staff(name = "Roy Ruddle"))
        db.session.add(Staff(name = "David Hogg"))
        db.session.add(Staff(name = "Natasha Shakhlevich"))
        db.session.add(Staff(name = "Jie Xu"))
        db.session.add(Staff(name = "Andy Bulpitt"))
        db.session.add(Staff(name = "Sam Wilson"))
        db.session.add(Staff(name = "Kelvin Lau"))
        db.session.add(Staff(name = "Farha Lakhani"))
        db.session.add(Staff(name = "Zheng Wang"))
        db.session.add(Staff(name = "Mehmet Dogar"))
        db.session.add(Staff(name = "Toni Lassila"))
        db.session.add(Staff(name = "He Wang"))
        db.session.add(Staff(name = "Mark Walkley"))
        db.session.add(Staff(name = "Eric Atwell"))
        db.session.add(Staff(name = "Abdulrahman Altahhan"))
        db.session.add(Staff(name = "Thomas Ranner"))
        db.session.add(Staff(name = "Amy Lowe"))
        db.session.add(Staff(name = "Netta Cohen"))
        db.session.add(Staff(name = "Joanna Leng"))
        db.session.add(Staff(name = "Tony Cohn"))
        db.session.add(Staff(name = "Raymond Kwan"))
        db.session.add(Staff(name = "Vania Dimitrova"))
        db.session.add(Staff(name = "Isolde Adler"))
        db.session.add(Staff(name = "John Stell"))

        db.session.commit()
        flash('A list of all supervisors has been added')
        return redirect(url_for('home'))
    except:
        flash('Supervisor name should be unique, check again')
        return redirect(url_for('home'))


@app.route('/default', methods=['GET', 'POST'])
def default():
    form = DefaultForm()
    if form.validate_on_submit():
        staff =  Staff.query.all()
        for s in staff:
            s.load = form.load.data
        db.session.commit()
        flash('Set default max load for all staff')
    return redirect(url_for('displayStaff'))


@app.route('/_autocompleteStaff', methods=['GET'])
def autocompleteStaff():
    staff_list = ['None']
    staff = Staff.query.all()
    for s in staff:
        staff_list.append(s.name)
    return Response(json.dumps(staff_list), mimetype='application/json')

@app.route('/_autocompleteProject', methods=['GET'])
def autocompleteProject():
    p_list = ['None']
    project = Project.query.all()
    for p in project:
        p_list.append(p.code)
    return Response(json.dumps(p_list), mimetype='application/json')


@app.route('/reports', methods=['GET', 'POST'])
def reports():
    return render_template('reports.html')

# labels used in pie chart
labels = [
    'Preference 1', 'Preference 2',
    'Preference 3', 'Preference 4',
    'Assigned but not a preference', 'Unassigned',
]

@app.route('/pieChart')
def pieChart():
    student = Student.query.count()
    stu1 = Student.query.filter_by(allocate=1).count()
    stu2 = Student.query.filter_by(allocate=2).count()
    stu3 = Student.query.filter_by(allocate=3).count()
    stu4 = Student.query.filter_by(allocate=4).count()
    stu5 = Student.query.filter_by(allocate=5).count()
    stu0 = student - (stu1 + stu2 + stu3 + stu4 + stu5)
    values = [stu1, stu2, stu3, stu4, stu5, stu0]

    r = []
    for i in range(len(labels)):
        item = {'labels': labels[i], 'values': values[i]}
        r.append(item)
    return json.dumps(r)


@app.route('/report1', methods=['GET', 'POST'])
def report1():
    # query for students that has been allocated a project and a supervisor
    student =  Student.query.filter(Student.projects!=None, Student.staff_name!=None).all()
    return render_template("report1.html",
                            title='Allocation by Student',
                            student=student)

@app.route('/report2', methods=['GET', 'POST'])
def report2():
    student =  Student.query.filter(Student.projects!=None, Student.staff_name!=None).all()
    return render_template("report2.html",
                            title='Allocation by Supervisor',
                            student=student)

@app.route('/report3', methods=['GET', 'POST'])
def report3():
    student =  Student.query.filter(Student.projects!=None, Student.staff_name!=None).all()
    return render_template("report3.html",
                            title='Allocation by Project',
                            student=student)

@app.route('/report4', methods=['GET', 'POST'])
def report4():
    staff = Staff.query.all()
    return render_template("report4.html",
                            title='Available Supervisors',
                            staff=staff)

@app.route('/report5')
def report5():
    return render_template("report5.html", title='Available Projects')


@app.route('/_dataReport5')
def dataReport5():
    return {'data': [project.to_dict() for project in Project.query]}
