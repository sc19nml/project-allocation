import os

# As we are using local web app, we do not need CSRF enabled
WTF_CSRF_ENABLED = False
SECRET_KEY = '1BCDEFGHIJKLMNOPQRSTUVWXYZ12345'


basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_TRACK_MODIFICATIONS = True
