#this python script imports the app package and then runs the development server with the debug flag set to true

from app import app
app.run(debug=True)
