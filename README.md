# Project allocation

This is a project for the allocation process of final year projects to students.

## Getting started

The developer built this programme using Python version 3 and Flask framework. Bellow is the instruction to install Flask:

## Installing Flask and Run the Application

It is recommended to install Flask in a virtual environment to avoid problems with conflicting libraries. A virtual environment is a tool which allows the separation of different python projects that requires different dependencies.

##### NOTE
These steps are only required once (first time running the programme), for future sessions you just need to activate the virtual environment then run the web application immediately (step 2 & 4).
- [ ] Install & Configure a Virtual Environment
- [ ] Activate the Environment
- [ ] Install Flask and the Dependencies
- [ ] Run the Web Application

### Step 1. Install & Configure a Virtual Environment
This step depends on your Python version
- If you have Python 3.5 or above, you do not need to install anything, as Python 3 comes with a virtual environment module named venv preinstalled.
Follow the instructions bellow to configure a virtual environment:
    - on Linux and MacOS:
    ```
    python3 -m venv <name of environment>
    ```
    - on Window:
    ```
    py -3 -m venv <name of environment>
    ```

- If you have Python 2, follow the instructions bellow to install and configure a virtual environment:
    - on MacOS:
    ```
    sudo python2 -m pip install virtualenv
    python -m virtualenv <name of environment>
    ```
    - on Linux:
    ```
    sudo apt install python-virtualenv
    python -m virtualenv <name of environment>
    ```
    - on Window:
    ```
    py -2 -m pip install virtualenv
    py -2 -m virtualenv <name of environment>
    ```

### Step 2. Activate the Environment
- Move into the directory that contains the environment folder
    - For Linux and MacOS:
    ```
    source <name of environment>/bin/activate
    ```

    - For Windows:
    ```
    <name of environment>\Scripts\activate
    ```

### Step 3. Install Flask and the Dependencies
- When the virtual environment is activated, move into your project directory where the requirements.txt file is located.
    ```
    pip install -r requirements.txt
    ```
- This requirements.txt contains all the dependencies needed for this project.


### Step 4. Run the Web Application
Now the host environment is ready to run the Flask app.
- Move into your project directory where run.py file is located

    - For Linux and MacOS:
    ```
    export FLASK_APP=run.py
    export FLASK_ENV=development
    flask run
    ```
    - For Windows:
    ```
    setx FLASK_APP "run.py"
    flask run
    ```

- FLASK_APP environment variable tells Flask which python script to run.
- FLASK_ENV variable tells Flask you are in development mode.
- flask run: run the Flask application
