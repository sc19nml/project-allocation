import os
import unittest

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from app import app, db, models, functions
from app.models import Student, Staff, Project
from app.functions import staffForCode, normaliseCode, swap
from config import basedir

class TestBase(unittest.TestCase):
    def setUp(self):
        app.config.from_object('config')
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'test.db')
        self.app = app.test_client()
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def populate_db(self):
        student = Student(stu_name='full name 1', username='poiuy', pref1 = 'ab1', pref2 = 'ab2', pref3 = 'ab3', pref4 = 'ab4')
        db.session.add(student)
        staff = Staff(name='josh', load=5)
        db.session.add(staff)
        project = Project(code='ab1')
        db.session.add(project)
        db.session.commit()

class TestDatabaseCreation(TestBase):

    def test_studentModel(self):
        """
        # Test number of records in Employee table
        """
        self.assertEqual(Student.query.count(), 1)

    def test_staffModel(self):
        """
        Test number of records in Department table
        """
        self.assertEqual(Staff.query.count(), 1)

    def test_projectModel(self):
        """
        Test number of records in Role table
        """
        self.assertEqual(Project.query.count(), 1)

class TestDatabaseUpdate(TestBase):

    def test_setDefaultLoad(self):
        """
        Test if defaut max load can be updated and set to all staff
        """
        response = self.app.post('/default', data={'load':'6'}, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert 'Set default max load for all staff' in html
        staff = Staff.query.filter_by(name='josh').first()
        self.assertEqual(staff.load,6)

    def test_editStudent(self):
        """
        Test if student can be edited according to the submitted data
        """
        response = self.app.post('/editStudent/1', data={'pref2':'ab_edited'}, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert 'Changes saved' in html
        student = Student.query.filter_by(id=1).first()
        self.assertEqual(student.pref2,'ab_edited')

    def test_addOneStudent(self):
        """
        Test if student can be added to datatable
        """
        response = self.app.post('/addOneStudent', data={'name':'andy', 'username':'ady',
        'pref1':'ad1','pref2':'ad2', 'pref3': 'ad3', 'pref4': 'ad4'}, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert 'andy added successfully' in html
        student = Student.query.filter_by(stu_name='andy').count()
        self.assertEqual(student,1)

    def test_editStaff(self):
        """
        Test if a staff record can be edited according to the submitted data
        """
        response = self.app.post('/editStaff/1', data={'load':'10'}, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert 'Changes saved' in html
        staff = Staff.query.filter_by(id=1).first()
        self.assertEqual(staff.load, 10)

    def test_addOneStaff(self):
        """
        Test if staff can be added to datatable
        """
        response = self.app.post('/addOneStaff', data={'name':'may', 'load':'8'}, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert 'may added successfully' in html
        staff = Staff.query.filter_by(name='may').count()
        self.assertEqual(staff,1)

    def test_editProject(self):
        """
        Test if a project record can be edited according to the submitted data
        """
        response = self.app.post('/editProject/1', data={'title':'No Name'}, follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert 'Changes saved' in html
        project = Project.query.filter_by(id=1).first()
        self.assertEqual(project.title, 'No Name')

class TestPreDefinedFunctions(TestBase):

    def test_staffForCode(self):
        """
        Test if function return the corresponding staff member name
        """
        code = 'AJB'
        self.assertEqual(staffForCode( code ), 'Andy Bulpitt')

    def test_normaliseCode(self):
        """
        Test if code can be normalsed as expected
        """
        code = '0. BA201 '
        self.assertEqual(normaliseCode(code), 'BA201')

    def test_swap(self):
        """
        Test if long project code can be shorten to a unique code
        """
        code = 'smart watch for students'
        swapped_code = swap(code)
        self.assertEqual(len(swapped_code), 8)
        self.assertEqual(Project.query.filter_by(code=swapped_code).count(),0)


class TestFormDisplay(TestBase):
    def test_studentAllocationForm(self):
        """
        Test if the allocation form in the web page is displayed
        """
        response = self.app.get('/student/poiuy', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert "OWN project" in html
        # make sure all the fields are included
        assert 'name="project"' in html
        assert 'name="supervisor"' in html

    def test_editStaffForm(self):
        """
        Test if the edit Staff form in the web page is displayed
        """
        response = self.app.get('/editStudent/1', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert 'name="pref1"' in html
        assert 'name="pref2"' in html
        assert 'name="pref3"' in html
        assert 'name="pref4"' in html

    def test_editStudentForm(self):
        """
        Test if the edit Student form in the web page is displayed
        """
        response = self.app.get('/editStaff/1', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert 'name="name"' in html
        assert 'name="load"' in html

    def test_editProjectForm(self):
        """
        Test if the edit Project form in the web page is displayed
        """
        response = self.app.get('/editProject/1', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert 'name="title"' in html
        assert 'name="load"' in html
        assert 'name="staff_proposed"' in html


class TestUserInputValidation(TestBase):

    def test_allocation_validInput(self):
        response = self.app.post('/student/poiuy', data={
            'project': 'ab1',
            'supervisor': 'josh',
        }, follow_redirects=True)
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Allocate successfully" in html

    def test_allocation_invalidInput(self):
        response = self.app.post('/student/poiuy', data={
            'project': '',
            'supervisor': '',
        }, follow_redirects=True)
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Project name and supervisor name required" in html

    def test_allocation_nonexistedStaffName(self):
        response = self.app.post('/student/poiuy', data={
            'project': 'ab1',
            'supervisor': 'nonexisted',
        }, follow_redirects=True)
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Warning: Staff name does not exist!" in html

    def test_allocation_nonexistedProjectCode(self):
        response = self.app.post('/student/poiuy', data={
            'project': 'nonexisted',
            'supervisor': 'josh',
        }, follow_redirects=True)
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "New project has been added to database" in html

    def test_deallocation_valid(self):
        response = self.app.post('/student/poiuy', data={
            'project': 'ab1',
            'supervisor': 'josh',
        }, follow_redirects=True)
        response = self.app.post('/student/poiuy', data={
            'project': 'None',
            'supervisor': 'None',
        }, follow_redirects=True)
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Deallocate for " in html
        s = Student.query.filter_by(username='poiuy').first()
        self.assertEqual(s.projects, None)
        self.assertEqual(s.staff_name, None)


    def test_deallocation_invalid(self):
        student = Student(stu_name='full name 2', username='tabata', pref1 = 'aa1', pref2 = 'aa2', pref3 = 'aa3', pref4 = 'aa4')
        db.session.add(student)
        db.session.commit()
        response = self.app.post('/student/tabata', data={
            'project': 'None',
            'supervisor': 'None',
        }, follow_redirects=True)
        assert response.status_code == 200
        html = response.get_data(as_text=True)
        assert "Deallocate does not work: this student have not yet assigned to any project" in html



class TestViews(TestBase):

    def test_homepage(self):
        """
        Test the route to homepage
        """
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert "Upload your CSV file" in html

    def test_displayStaff(self):
        """
        Test that the link to the staff table is accessible
        """
        response = self.app.get('/staff', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert "Supervisors and their availability" in html

    def test_displayStudent(self):
        """
        Test that the link to the student table is accessible
        """
        response = self.app.get('/student', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert "Student's preferences & Allocation table" in html

    def test_displayProject(self):
        """
        Test that the link to the project table is accessible
        """
        response = self.app.get('/project', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        html = response.get_data(as_text=True)
        assert "Project list and its availability" in html

    def test_autocompleteStaff(self):
        """
        Test that the a list of staff is returned for autocomplete
        """
        response = self.app.get('/_autocompleteStaff')
        self.assertEqual(response.status_code, 200)

    def test_autocompleteProject(self):
        """
        Test that the a list of project is returned for autocomplete
        """
        response = self.app.get('/_autocompleteStaff')
        self.assertEqual(response.status_code, 200)

    def test_dataReport5(self):
        """
        Test that the a list of project is returned for autocomplete
        """
        response = self.app.get('/_dataReport5')
        self.assertEqual(response.status_code, 200)

    def test_report5(self):
        """
        Test that the a list of project is returned for autocomplete
        """
        response = self.app.get('/report5')
        self.assertEqual(response.status_code, 200)

    def test_report4(self):
        """
        Test that the a list of project is returned for autocomplete
        """
        response = self.app.get('/report4')
        self.assertEqual(response.status_code, 200)

    def test_report3(self):
        """
        Test that the a list of project is returned for autocomplete
        """
        response = self.app.get('/report3')
        self.assertEqual(response.status_code, 200)

    def test_report2(self):
        """
        Test that the a list of project is returned for autocomplete
        """
        response = self.app.get('/report2')
        self.assertEqual(response.status_code, 200)

    def test_report1(self):
        """
        Test that the a list of project is returned for autocomplete
        """
        response = self.app.get('/report1')
        self.assertEqual(response.status_code, 200)

    def test_pieChart(self):
        """
        Test that the a list of project is returned for autocomplete
        """
        response = self.app.get('/pieChart')
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
