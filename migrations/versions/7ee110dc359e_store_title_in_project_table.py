"""store title in Project table

Revision ID: 7ee110dc359e
Revises: 4e1d4a504c48
Create Date: 2022-05-10 13:04:32.375046

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7ee110dc359e'
down_revision = '4e1d4a504c48'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('project', sa.Column('title', sa.String(length=100), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('project', 'title')
    # ### end Alembic commands ###
