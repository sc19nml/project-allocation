"""unique constrains on username

Revision ID: 5e07bf30e260
Revises: 4f04e6aadab1
Create Date: 2021-11-17 01:53:16.719888

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5e07bf30e260'
down_revision = '4f04e6aadab1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('ix_student_username', table_name='student')
    op.create_index(op.f('ix_student_username'), 'student', ['username'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_student_username'), table_name='student')
    op.create_index('ix_student_username', 'student', ['username'], unique=False)
    # ### end Alembic commands ###
